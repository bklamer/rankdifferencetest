## Test environments

- local ubuntu 20.04, R 4.1.2
- win-builder, R-devel 2021-11-22 r81222, windows-x86_64-w64-mingw32

## R CMD check results

0 errors | 0 warnings | 0 notes

## Downstream dependencies

There are currently no downstream dependencies for this package.
