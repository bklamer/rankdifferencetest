x <- rankdifferencetest:::parse_formula(formula = ~ x, data = list())
yx <- rankdifferencetest:::parse_formula(formula = y ~ x, data = list())
yxblock <- rankdifferencetest:::parse_formula(formula = y ~ x | b, data = list())

expect_equal(
  x,
  list(
    lhs = NULL, y_name = NULL, rhs = ~x, rhs_group = ~x, x_name = "x",
    rhs_block = NULL, block_name = NULL
  )
)

expect_equal(
  yx,
  list(
    lhs = ~y, y_name = "y", rhs = ~x, rhs_group = ~x, x_name = "x",
    rhs_block = NULL, block_name = NULL
  )
)

expect_equal(
  yxblock,
  list(
    lhs = ~y, y_name = "y", rhs = ~x | b, rhs_group = ~x, x_name = "x",
    rhs_block = ~b, block_name = "b"
  )
)
