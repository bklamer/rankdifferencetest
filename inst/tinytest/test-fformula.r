# Works as expected
expect_identical(rankdifferencetest:::fformula("~ x"), formula(~ x))
expect_identical(rankdifferencetest:::fformula("y ~ x"), formula(y ~ x))
expect_identical(rankdifferencetest:::fformula("y ~ x | b"), formula(y ~ x | b))
