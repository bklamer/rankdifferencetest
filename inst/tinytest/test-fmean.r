x <- c(1, 2)
xmissing <- c(1, 2, NA)

expect_equal(rankdifferencetest:::fmean(x), mean(x))
expect_equal(rankdifferencetest:::fmean(xmissing), NA_real_)
expect_equal(
  rankdifferencetest:::fmean(xmissing, na.rm = TRUE),
  mean(xmissing, na.rm = TRUE)
)
