# Data
data <- kornbrot_table1
data_long <- reshape(
  data = kornbrot_table1,
  direction = "long",
  varying = c("placebo", "drug"),
  v.names = c("time"),
  idvar = "subject",
  times = c("placebo", "drug"),
  timevar = "treatment",
  new.row.names = seq_len(prod(length(c("placebo", "drug")), nrow(kornbrot_table1)))
)
data_long$subject <- factor(data_long$subject)
data_long$treatment <- factor(data_long$treatment, levels = c("drug", "placebo"))
data_long$xtra <- seq_len(nrow(data_long))

#-------------------------------------------------------------------------------
# Argument checks return errors
#-------------------------------------------------------------------------------
expect_error(
  rdt(data = 1, formula = placebo ~ drug),
  pattern = "Argument 'data' must be an object of class 'data.frame'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = data),
  pattern = "Argument 'formula' must be an object of class 'formula'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, ci = "TRUE"),
  pattern = "Argument 'ci' be a scalar vector of class 'logical'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, ci = TRUE, ci.level = 1.1),
  pattern = "Argument 'ci.level' must be a scalar vector of class 'numeric' between 0 and 1.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, alternative = "two"),
  pattern = "Argument 'alternative' must be a scalar vector of class 'character'. Choose from 'two.sided', 'greater', or 'less'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, mu = "0"),
  pattern = "Argument 'mu' must be a scalar vector of class 'numeric'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, distribution = "Exact"),
  pattern = "Argument 'distribution' must be a scalar vector of class 'character'. Choose from 'exact' or 'asymptotic'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, correct = 1),
  pattern = "Argument 'correct' must be a scalar vector of class 'logical'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug, zero.method = "Wilcoxon"),
  pattern = "Argument 'zero.method' must be a scalar vector of class 'character'. Choose from 'pratt' or 'wilcoxon'.",
  fixed = TRUE
)

#-------------------------------------------------------------------------------
# Malformed formula returns errors
#-------------------------------------------------------------------------------
expect_error(
  rdt(data = data, formula = placebo ~ drug2),
  pattern = "Check argument 'formula'. It included variables not found in 'data': 'drug2'",
  fixed = TRUE
)
expect_error(
  rdt(data = data, formula = placebo ~ drug + subject),
  pattern = "Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.",
  fixed = TRUE
)
expect_error(
  rdt(data = data_long, formula = time ~ treatment + xtra | subject),
  pattern = "Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.",
  fixed = TRUE
)
# Need better error message here?
expect_error(
  rdt(data = data_long, formula = time ~ treatment | xtra),
  pattern = "The number of non-missing observations (sample size) was less than 2.",
  fixed = TRUE
)
expect_error(
  rdt(data = data_long, formula = time ~ treatment | treatment),
  pattern = "Check argument 'formula'. It contained a duplicated variable: time ~ treatment | treatment",
  fixed = TRUE
)

#-------------------------------------------------------------------------------
# p-value matches Kornbrot 1990
#-------------------------------------------------------------------------------
# tall format expected result
expect_equal(
  rdt(
    data = data_long,
    formula = time ~ treatment | subject,
    alternative = "greater",
    distribution = "asymptotic",
    zero.method = "wilcoxon",
    correct = FALSE
  )$p.value,
  0.1238529,
  tolerance = 1e-6
)

# wide format expected result
expect_equal(
  rdt(
    data = data,
    formula = placebo ~ drug,
    alternative = "greater",
    distribution = "asymptotic",
    zero.method = "wilcoxon",
    correct = FALSE
  )$p.value,
  0.1238529,
  tolerance = 1e-6
)

# transformed wide format
expect_equal(
  rdt(
    data = data,
    formula = I(60/placebo) ~ I(60/drug),
    alternative = "less",
    distribution = "asymptotic",
    zero.method = "wilcoxon",
    correct = FALSE
  )$p.value,
  0.1238529,
  tolerance = 1e-6
)

# transformed tall format
expect_equal(
  rdt(
    data = data_long,
    formula = I(60/time) ~ treatment | subject,
    alternative = "less",
    distribution = "asymptotic",
    zero.method = "wilcoxon",
    correct = FALSE
  )$p.value,
  0.1238529,
  tolerance = 1e-6
)

#-------------------------------------------------------------------------------
# Blocking handles random rows
#-------------------------------------------------------------------------------
data_long2 <- data_long[sample(seq_len(nrow(data_long)), nrow(data_long)), ]
res1 <- rdt(
  data = data_long,
  formula = time ~ treatment | subject,
  distribution = "asymptotic",
  zero.method = "wilcoxon",
  correct = FALSE
)
res2 <- rdt(
  data = data_long2,
  formula = time ~ treatment | subject,
  distribution = "asymptotic",
  zero.method = "wilcoxon",
  correct = FALSE
)

expect_equal(
  res1$p.value,
  res2$p.value
)

#-------------------------------------------------------------------------------
# Return summaries are correct
# 1. Formula
#     - y ~ x
#     - y ~ x | block
# 2. Alternative
#     - two.sided
#     - greater
#     - less
# 3. Distribution
#     - exact
#     - asymptotic
#     - approximate
# 4. zero.method
#     - Wilcoxon
#     - Pratt
#-------------------------------------------------------------------------------
# Correct formula
res <- rdt(
  data = data,
  formula = placebo ~ drug
)
res2 <- rdt(
  data = data_long,
  formula = time ~ treatment | subject
)
expect_equal(
  res$formula,
  placebo ~ drug
)
expect_equal(
  res2$formula,
  time ~ treatment | subject
)

# correct alternative
res <- rdt(
  data = data,
  formula = placebo ~ drug,
  alternative = "two.sided"
)
res2 <- rdt(
  data = data,
  formula = placebo ~ drug,
  alternative = "greater"
)
res3 <- rdt(
  data = data,
  formula = placebo ~ drug,
  alternative = "less"
)
expect_equal(
  res$alternative,
  "True location shift of differences in ranks 'placebo - drug' is not equal to 0"
)
expect_equal(
  res2$alternative,
  "True location shift of differences in ranks 'placebo - drug' is greater than 0"
)
expect_equal(
  res3$alternative,
  "True location shift of differences in ranks 'placebo - drug' is less than 0"
)

# correct method
res <- rdt(
  data = data,
  formula = placebo ~ drug,
  distribution = "exact"
)
res2 <- rdt(
  data = data,
  formula = placebo ~ drug,
  distribution = "asymptotic"
)
expect_equal(
  res$method,
  "Kornbrot's rank difference test using the exact Wilcoxon signed-rank test using the Shift-algorithm"
)
expect_equal(
  res2$method,
  "Kornbrot's rank difference test using the asymptotic Wilcoxon signed-rank test with continuity correction"
)

# correct zero-difference method
res <- rdt(
  data = data,
  formula = placebo ~ drug,
  zero.method = "wilcoxon"
)
res2 <- rdt(
  data = data,
  formula = placebo ~ drug,
  zero.method = "pratt"
)
expect_equal(
  res$method,
  "Kornbrot's rank difference test using the exact Wilcoxon signed-rank test using the Shift-algorithm"
)
expect_equal(
  res2$method,
  "Kornbrot's rank difference test using the exact Wilcoxon-Pratt signed-rank test using the Shift-algorithm"
)
