set.seed(123)
data <- data.frame(y = rnorm(6), x = rnorm(6))
datablock <- data.frame(
  y = rnorm(6),
  x = gl(2, 3, labels = c("pre", "post")),
  b = rep(1:3, times = 2)
)

expect_error(
  rankdifferencetest:::rdt_data_prep(data = data, formula = ~ x),
  pattern = "Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'."
)

expect_error(
  rankdifferencetest:::rdt_data_prep(data = data, formula = y ~ x | x),
  pattern = "Check argument 'formula'. It contained a duplicated variable: y ~ x | x"
)

expect_error(
  rankdifferencetest:::rdt_data_prep(data = datablock, formula = y ~ x | block),
  pattern = "Check argument 'formula'. It included variables not found in 'data': 'block'"
)

expect_error(
  rankdifferencetest:::rdt_data_prep(data = datablock, formula = y ~ x),
  pattern = "In argument 'formula', variable 'x' should have been numeric."
)
