x <- c(1, 2)
xmissing <- c(1, 2, NA)

expect_equal(rankdifferencetest:::fmedian(x), median(x))
#expect_equal(rankdifferencetest:::fmedian(xmissing), NA_real_)
expect_equal(
  rankdifferencetest:::fmedian(xmissing, na.rm = TRUE),
  median(xmissing, na.rm = TRUE)
)
