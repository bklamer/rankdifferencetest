x <- c(1, 2)
y <- c(3, 4)

xmissing <- c(1, 2, NA)
ymissing <- c(4, 3, NA)

expect_equal(rankdifferencetest:::fouter_add(x, y), outer(x, y, "+"))
expect_equal(
  rankdifferencetest:::fouter_add(xmissing, ymissing),
  outer(xmissing, ymissing, "+")
)
