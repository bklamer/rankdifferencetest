res <- rep(NA, 100)
for(i in 1:100) {
  x <- sample(-200:200, 100, replace = TRUE)
  res[i] <- all(
    rankdifferencetest:::ftabulate(x) == rle(sort(x))[["lengths"]] &
    rankdifferencetest:::ftabulate(x) == table(x)
  )
}

expect_true(all(res))
