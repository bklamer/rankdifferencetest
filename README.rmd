---
output: github_document
editor_options: 
  chunk_output_type: console
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# rankdifferencetest

`rankdifferencetest` provides a modified Wilcoxon signed-rank test which produces 
consistent and meaningful results for ordinal or monotonically transformed data.
The procedure was described by Diana Kornbrot in <https://doi.org/10.1111/j.2044-8317.1990.tb00939.x>

Options for the signed-rank test include distribution ('asymptotic' or 'exact'), 
continuity correction, and zero handling ('wilcoxon' or 'pratt').

## Installation

Install from CRAN:

```{r, eval = FALSE, include = TRUE}
install.packages("rankdifferencetest")
```

Or browse the code at <https://bitbucket.org/bklamer/rankdifferencetest> and 
install the development version using:

```{r, eval = FALSE, include = TRUE}
devtools::install_bitbucket("bklamer/rankdifferencetest")
```

## Examples

```{r, eval = TRUE, include = TRUE}
# See ?rdt and ?kornbrot_table1
library(rankdifferencetest)

res <- rdt(
  data = kornbrot_table1,
  formula = placebo ~ drug,
  ci = TRUE,
  alternative = "two.sided",
  distribution = "asymptotic",
  zero.method = "wilcoxon",
  correct = FALSE
)
res
```

## License

Copyright: Brett Klamer - 2021 - MIT (http://opensource.org/licenses/MIT)
