
# rankdifferencetest

`rankdifferencetest` provides a modified Wilcoxon signed-rank test which
produces consistent and meaningful results for ordinal or monotonically
transformed data. The procedure was described by Diana Kornbrot in
<https://doi.org/10.1111/j.2044-8317.1990.tb00939.x>

Options for the signed-rank test include distribution (‘asymptotic’ or
‘exact’), continuity correction, and zero handling (‘wilcoxon’ or
‘pratt’).

## Installation

Install from CRAN:

``` r
install.packages("rankdifferencetest")
```

Or browse the code at <https://bitbucket.org/bklamer/rankdifferencetest>
and install the development version using:

``` r
devtools::install_bitbucket("bklamer/rankdifferencetest")
```

## Examples

``` r
# See ?rdt and ?kornbrot_table1
library(rankdifferencetest)

res <- rdt(
  data = kornbrot_table1,
  formula = placebo ~ drug,
  ci = TRUE,
  alternative = "two.sided",
  distribution = "asymptotic",
  zero.method = "wilcoxon",
  correct = FALSE
)
res
#> $statistic
#>       Z 
#> 1.15594 
#> 
#> $p.value
#> [1] 0.2477059
#> 
#> $alternative
#> [1] "True location shift of differences in ranks 'placebo - drug' is not equal to 0"
#> 
#> $method
#> [1] "Kornbrot's rank difference test using the asymptotic Wilcoxon signed-rank test without continuity correction"
#> 
#> $formula
#> placebo ~ drug
#> 
#> $pseudomedian
#> $pseudomedian$estimate
#> [1] 1.500004
#> 
#> $pseudomedian$lower
#> [1] -2.499915
#> 
#> $pseudomedian$upper
#> [1] 5.999958
#> 
#> $pseudomedian$ci.level.requested
#> [1] 0.95
#> 
#> $pseudomedian$ci.level.achieved
#> [1] NA
#> 
#> $pseudomedian$estimate.method
#> [1] "pseudomedian based on root-finding algorithm for the asymptotic Wilcoxon signed-rank distribution"
#> 
#> $pseudomedian$ci.method
#> [1] "quantiles based on root-finding algorithm for the asymptotic Wilcoxon signed-rank distribution"
#> 
#> 
#> $n
#> $n$original
#> [1] 13
#> 
#> $n$nonmissing
#> [1] 13
#> 
#> $n$zero.adjusted
#> [1] 13
#> 
#> $n$zeros
#> [1] 0
#> 
#> $n$ties
#> [1] 6
#> 
#> 
#> attr(,"class")
#> [1] "rankdifferencetest" "list"
```

## License

Copyright: Brett Klamer - 2021 - MIT
(<http://opensource.org/licenses/MIT>)
