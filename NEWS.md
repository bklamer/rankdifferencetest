# rankdifferencetest (development version)

- Major changes
  - Refactored `rdt()`.
      - Warning! Old code will break. Changes in arguments and result object 
        for `rdt()`.
      - `rdt()` no longer depends on `coin::wilcoxsign_test()` and is at least 
        an order of magnitude faster.
  
- Additions
  - Function `srt()` for the Wilcoxon signed-rank test.
  - Functions `as.data.frame()` and `tidy()` for `rankdifferencetest` objects 
    will automatically coerce from `list` to `data.frame` in typical 
    `broom::tidy()` style
  
# rankdifferencetest 2021-11-25

- First release of `rankdifferencetest`.
