#' Coerce to a data frame
#'
#' Coerce's a `rankdifferencetest` object to a data frame.
#'
#' @param x A `rankdifferencetest` object.
#' @param ... Unused arguments.
#'
#' @return `data.frame`
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # as.data.frame() and tidy() examples
#' #----------------------------------------------------------------------------
#' library(rankdifferencetest)
#'
#' # Use example data from Kornbrot (1990)
#' data <- kornbrot_table1
#'
#' rdt(
#'   data = data,
#'   formula = placebo ~ drug,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   zero.method = "wilcoxon",
#'   correct = FALSE,
#'   ci = TRUE
#' ) |> as.data.frame()
#'
#' rdt(
#'   data = data,
#'   formula = placebo ~ drug,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   zero.method = "wilcoxon",
#'   correct = FALSE,
#'   ci = TRUE
#' ) |> tidy()
#'
#' @rdname as.data.frame
#' @export
as.data.frame.rankdifferencetest <- function(x, ...) {
  if(is.na(x$pseudomedian$estimate)) {
    res <- list(
      x$statistic,
      x$p.value,
      x$method,
      x$alternative
    )

    attr(res, "names") <- c("statistic", "p.value", "method", "alternative")
    attr(res, "row.names") <- .set_row_names(1L)
    attr(res, "class") <- "data.frame"
  } else {
    res <- list(
      x$pseudomedian$estimate,
      x$statistic,
      x$p.value,
      x$pseudomedian$lower,
      x$pseudomedian$upper,
      x$method,
      x$alternative
    )

    attr(res, "names") <- c(
      "estimate", "statistic", "p.value", "conf.low", "conf.high", "method",
      "alternative"
    )
    attr(res, "row.names") <- .set_row_names(1L)
    attr(res, "class") <- "data.frame"
  }

  # Return
  res
}

#' @rdname as.data.frame
#' @export
tidy.rankdifferencetest <- function(x, ...) {
  if(is.na(x$pseudomedian$estimate)) {
    res <- list(
      x$statistic,
      x$p.value,
      x$method,
      x$alternative
    )

    attr(res, "names") <- c("statistic", "p.value", "method", "alternative")
    attr(res, "row.names") <- .set_row_names(1L)
    attr(res, "class") <- "data.frame"
  } else {
    res <- list(
      x$pseudomedian$estimate,
      x$statistic,
      x$p.value,
      x$pseudomedian$lower,
      x$pseudomedian$upper,
      x$method,
      x$alternative
    )

    attr(res, "names") <- c(
      "estimate", "statistic", "p.value", "conf.low", "conf.high", "method",
      "alternative"
    )
    attr(res, "row.names") <- .set_row_names(1L)
    attr(res, "class") <- "data.frame"
  }

  # Return
  res
}

#' @importFrom generics tidy
#' @export
generics::tidy
