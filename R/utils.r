#-------------------------------------------------------------------------------
# Fast modeltools::ParseFormula()
# Modified from modeltools::ParseFormula() v0.2-23 by Friedrich Leisch, GPL-2
#-------------------------------------------------------------------------------
#' @importFrom stats terms
parse_formula <- function(formula, data) {
  formula <- terms(x = formula, data = data)
  attributes(formula) <- NULL

  # length==3 if lhs ~ rhs
  # length==2 if ~ rhs
  if(length(formula) == 3L) {
    lhs <- formula[c(1L, 2L)]
    rhs <- formula[c(1L, 3L)]
  } else if(length(formula) == 2L) {
    lhs <- NULL
    rhs <- formula
  }

  rhs_group <- rhs
  rhs_block <- rhs

  if(!is.null(rhs) && length(rhs[[2L]]) > 1L) {
    if(deparse(rhs[[2]][[1]]) == "|") {
      rhs_group[[2]] <- rhs[[2]][[2]]
      rhs_block[[2]] <- rhs[[2]][[3]]
    } else {
      rhs_block <- NULL
    }
  } else {
    rhs_block <- NULL
  }

  # Return
  list(
    lhs = lhs,
    y_name = if(is.null(lhs)) lhs else as.character(lhs[[2]]),
    rhs = rhs,
    rhs_group = rhs_group,
    x_name = as.character(rhs_group[[2]]),
    rhs_block = rhs_block,
    block_name = if(is.null(rhs_block)) rhs_block else as.character(rhs_block[[2]])
  )
}

#-------------------------------------------------------------------------------
# Fast base::tabulate(as.factor(x))
# Counts the number of times each unique value occurs in a vector.
# If all missing, returns zero. If some missing automatically excluded.
# For the purpose of ties, e.g. 1 = zero ties, 2 = one tie, 3 = two ties, etc.
# Modified from base::rle() and base::diff() v4.4.1 by R Core Team, GPL-2
#-------------------------------------------------------------------------------
ftabulate <- function(x, n = length(x), is.sorted = FALSE) {
  if(!is.sorted) {x <- sort(x)}
  y <- x[-1L] != x[-n]
  i <- c(0L, which(y), n)
  i[-1L] - i[-length(i)]
}

#-------------------------------------------------------------------------------
# Fast base::mean()
#-------------------------------------------------------------------------------
fmean <- function(x, n = length(x), na.rm = FALSE) {
  if(na.rm) {
    x <- x[!is.na(x)]
    n <- length(x)
    sum(x) / n
  } else {
    sum(x) / n
  }
}

#-------------------------------------------------------------------------------
# Fast stats:::median.default()
# Modified from stats:::median.default() v4.4.1 by R Core Team, GPL-2
#-------------------------------------------------------------------------------
fmedian <- function(x, n = length(x), is.sorted = FALSE, na.rm = FALSE) {
  if(na.rm) {
    x <- x[!is.na(x)]
    n <- length(x)

    # (n + 1L) %/% 2L is slower, but returns integer
    half <- ceiling(n / 2)

    if(is.sorted) {
      if(n%%2L == 1L) {
        # middle
        x[half]
      } else {
        # mean
        fmean(x[half + 0L:1L], n = 2)
      }
    } else {
      if(n%%2L == 1L) {
        # middle
        sort(x, partial = half)[half]
      } else {
        # mean
        fmean(sort(x, partial = half + 0L:1L)[half + 0L:1L], n = 2)
      }
    }
  } else {
    # (n + 1L) %/% 2L is slower, but returns integer
    half <- ceiling(n / 2)

    if(is.sorted) {
      if(n%%2L == 1L) {
        # middle
        x[half]
      } else {
        # mean
        fmean(x[half + 0L:1L], n = 2)
      }
    } else {
      # Careful with NAs. sort will remove them. n would then be wrong.
      if(n%%2L == 1L) {
        # middle
        sort(x, partial = half)[half]
      } else {
        # mean
        fmean(sort(x, partial = half + 0L:1L)[half + 0L:1L], n = 2)
      }
    }
  }
}

#-------------------------------------------------------------------------------
# Simple base::outer(x, y, FUN = "+")
# Modified from base::outer() v4.4.1 by R Core Team, GPL-2
#-------------------------------------------------------------------------------
fouter_add <- function(x, y, nx = length(x), ny = length(y)) {
  y <- rep(y, rep.int(nx, ny))
  x <- rep(x, times = ceiling(length(y)/nx))
  res <- x + y
  dim(res) <- c(nx, ny)
  res
}

#-------------------------------------------------------------------------------
# Fast formula
# Modified from stats::formula() v4.4.1 by R Core Team, GPL-2
#-------------------------------------------------------------------------------
fformula <- function(x, env = parent.frame()) {
  x <- str2lang(x)
  class(x) <- "formula"
  environment(x) <- env
  x
}
