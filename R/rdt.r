#' Rank difference test
#'
#' Performs Kornbrot's rank difference test, which is a modified Wilcoxon
#' signed-rank test that produces consistent and meaningful results for ordinal
#' or monotonically transformed data.
#'
#' For paired data, the Wilcoxon signed-rank test results in subtraction of the
#' paired values. However, this subtraction is not meaningful for ordinal scale
#' variables. In addition, any monotone transformation of the data will result
#' in different signed ranks, thus different p-values. However, ranking the
#' original data allows for meaningful addition and subtraction of ranks and
#' preserves ranks over monotonic transformation. Kornbrot developed the rank
#' difference test for these reasons.
#'
#' Kornbrot recommends that the rank difference test be used in preference to
#' the Wilcoxon signed-rank test in all paired comparison designs where the data
#' are not both of interval scale and of known distribution. The rank difference
#' test preserves good power compared to Wilcoxon's signed-rank test, is more
#' powerful than the sign test, and has the benefit of being a true
#' distribution-free test.
#'
#' The procedure for Kornbrot's rank difference test is as follows:
#'
#' 1. Combine all \eqn{2n} paired observations.
#' 2. Order the values from smallest to largest.
#' 3. Assign ranks \eqn{1, 2, \dots, 2n} with average rank for ties.
#' 4. Perform the Wilcoxon signed-rank test using the paired ranks.
#'
#' The test statistic for the rank difference test \eqn{(D)} is not exactly
#' equal to the test statistic of the naive rank-transformed Wilcoxon
#' signed-rank test \eqn{(W^+)}, the latter being implemented in
#' [rankdifferencetest::rdt()]. Using \eqn{W^+} should result in a
#' conservative estimate for \eqn{D}, and they approach in distribution as the
#' sample size increases. \insertCite{kornbrot1990;textual}{rankdifferencetest}
#' discusses methods for calculating \eqn{D} when \eqn{n<7} and
#' \eqn{8 < n \leq 20}.
#'
#' See [rankdifferencetest::srt()] for additional details about implementation
#' of Wilcoxon's signed-rank test.
#'
#' @param data A data frame.
#' @param formula A formula of form:
#'        \describe{
#'          \item{y ~ x | block}{
#'            Use when `data` is in tall format. `y` is the numeric outcome,
#'            `x` is the binary explanatory variable, and `block` is the
#'            subject/item-level variable. If `x` is a factor, the first level
#'            will be the reference value. e.g.,
#'            `levels(data$x) <- c("pre", "post")` will result in the difference
#'            `post - pre`.
#'          }
#'          \item{y ~ x}{
#'            Use when `data` is in wide format. Differences are calculated
#'            as `data$y - data$x`.
#'          }
#'        }
#' @param ci A scalar logical. Whether or not to calculate the pseudomedian and
#'        its confidence interval.
#' @param ci.level A scalar numeric from (0, 1). The confidence level.
#' @param alternative A string for the alternative hypothesis: `"two.sided"`
#'        (default), `"greater"`, or `"less"`.
#' @param mu A scalar numeric from (-Inf, Inf). Under the null hypothesis, `x`
#'        or `x - y` is assumed to be symmetric around `mu`.
#' @param distribution A string for the method used to calculate the p-value.
#'        If `"exact"`, the exact Wilcoxon signed-rank distribution is used.
#'        If `"asymptotic"`, the asymptotic normal approximation is used. The
#'        default (`NULL`) will automatically choose an appropriate method
#'        (`distribution = "exact"` when n < 50 or `distribution = "asymptotic"`
#'        otherwise).
#' @param correct A scalar logical. Whether or not to apply a continuity
#'        correction for the normal approximation of the p-value.
#' @param zero.method A string for the method used to handle values equal to
#'        zero: `"wilcoxon"` (default) or `"pratt"`.
#' @param tol.root A numeric scalar from (0, Inf). For
#' [stats::uniroot]`(*, tol=tol.root)` calls when `ci = TRUE` and
#'        `distribution = "asymptotic"`.
#'
#' @seealso [rankdifferencetest::srt()]
#'
#' @references
#' \insertRef{kornbrot1990}{rankdifferencetest}
#'
#' @return
#' A list with the following elements:
#' \tabular{llll}{
#'   Slot \tab Subslot \tab Name \tab Description \cr
#'   1 \tab \tab `statistic` \tab Test statistic. \eqn{W^+} for the exact Wilcoxon signed-rank distribution or \eqn{Z} for the asymptotic normal approximation. \cr
#'   2 \tab \tab `p` \tab p-value. \cr
#'   3 \tab \tab `alternative` \tab The alternative hypothesis. \cr
#'   4 \tab \tab `method` \tab Method used for test results. \cr
#'   5 \tab \tab `formula` \tab Model formula. \cr
#'
#'   6 \tab   \tab `pseudomedian` \tab Measure of centrality for `y - x`. Not calculated when argument `ci = FALSE`. \cr
#'   6 \tab 1 \tab `estimate` \tab Estimated pseudomedian. \cr
#'   6 \tab 2 \tab `lower` \tab Lower bound of confidence interval for the pseudomedian. \cr
#'   6 \tab 3 \tab `upper` \tab Upper bound of confidence interval for the pseudomedian. \cr
#'   6 \tab 4 \tab `ci.level.requested` \tab The chosen `ci.level`. \cr
#'   6 \tab 5 \tab `ci.level.achieved` \tab For pathological cases, the achievable confidence level. \cr
#'   6 \tab 6 \tab `estimate.method` \tab Method used for calculating the pseudomedian. \cr
#'   6 \tab 7 \tab `ci.method` \tab Method used for calculating the confidence interval. \cr
#'
#'   7 \tab   \tab `n` \tab Number of observations \cr
#'   7 \tab 1 \tab `original` \tab The number of observations contained in `data`. \cr
#'   7 \tab 2 \tab `nonmissing` \tab The number of non-missing observations available for analysis. \cr
#'   7 \tab 3 \tab `zero.adjusted` \tab The number of non-missing and non-zero values available for analysis. i.e. `n$nonmissing - n$zeros`. \cr
#'   7 \tab 4 \tab `zeros` \tab The number of values that were zero. \cr
#'   7 \tab 5 \tab `ties` \tab The number of values that were tied.
#' }
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # rdt() example
#' #----------------------------------------------------------------------------
#' library(rankdifferencetest)
#'
#' # Use example data from Kornbrot (1990)
#' data <- kornbrot_table1
#'
#' # Create long-format data for demonstration purposes
#' data_long <- reshape(
#'   data = kornbrot_table1,
#'   direction = "long",
#'   varying = c("placebo", "drug"),
#'   v.names = c("time"),
#'   idvar = "subject",
#'   times = c("placebo", "drug"),
#'   timevar = "treatment",
#'   new.row.names = seq_len(prod(length(c("placebo", "drug")), nrow(kornbrot_table1)))
#' )
#'
#' # Subject and treatment should be factors. The ordering of the treatment factor
#' # will determine the difference (placebo - drug).
#' data_long$subject <- factor(data_long$subject)
#' data_long$treatment <- factor(data_long$treatment, levels = c("drug", "placebo"))
#'
#' # Recreate analysis and results from section 7.1 in Kornbrot (1990)
#' ## The p-value shown in Kornbrot (1990) was continuity corrected. rdt() does
#' ## not apply a continuity correction, so the p-value here will be slightly
#' ## lower. It does match the uncorrected p-value shown in footnote on page 246.
#' rdt(
#'   data = data,
#'   formula = placebo ~ drug,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   zero.method = "wilcoxon",
#'   correct = FALSE,
#'   ci = TRUE
#' )
#' rdt(
#'   data = data_long,
#'   formula = time ~ treatment | subject,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   zero.method = "wilcoxon",
#'   correct = FALSE,
#'   ci = TRUE
#' )
#'
#' # The same outcome is seen after transforming time to rate.
#' ## The rate transformation inverts the rank ordering.
#' data$placebo_rate <- 60 / data$placebo
#' data$drug_rate <- 60 / data$drug
#' data_long$rate <- 60 / data_long$time
#'
#' rdt(
#'   data = data,
#'   formula = placebo_rate ~ drug_rate,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   zero.method = "wilcoxon",
#'   correct = FALSE,
#'   ci = TRUE
#' )
#' rdt(
#'   data = data_long,
#'   formula = rate ~ treatment | subject,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   zero.method = "wilcoxon",
#'   correct = FALSE,
#'   ci = TRUE
#' )
#'
#' # In contrast to the rank difference test, the Wilcoxon signed-rank test
#' # produces differing results. See table 1 and table 2 in Kornbrot (1990).
#' wilcox.test(
#'   x = data$placebo,
#'   y = data$drug,
#'   alternative = "two.sided",
#'   paired = TRUE,
#'   exact = FALSE,
#'   correct = FALSE
#' )$p.value/2
#' wilcox.test(
#'   x = data$placebo_rate,
#'   y = data$drug_rate,
#'   alternative = "two.sided",
#'   paired = TRUE,
#'   exact = FALSE,
#'   correct = FALSE
#' )$p.value/2
#'
#' @export
rdt <- function(
    data,
    formula,
    ci = FALSE,
    ci.level = 0.95,
    alternative = "two.sided",
    mu = 0,
    distribution = NULL,
    correct = TRUE,
    zero.method = "wilcoxon",
    tol.root = 1e-04
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  if(!is.data.frame(data)) {
    stop("Argument 'data' must be an object of class 'data.frame'.")
  }
  if(!inherits(formula, "formula")) {
    stop("Argument 'formula' must be an object of class 'formula'.")
  }

  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  data_prep <- rdt_data_prep(data = data, formula = formula)

  data <- data_prep[["data"]]
  n_nonmissing <- data_prep[["n_nonmissing"]]

  if(n_nonmissing < 2L) {
    stop("The number of non-missing observations (sample size) was less than 2.")
  }

  #-----------------------------------------------------------------------------
  # Perform test
  #-----------------------------------------------------------------------------
  res <- srt(
    data = data,
    formula = fformula(paste0(names(data)[1L], " ~ ", names(data)[2L])),
    ci = ci,
    ci.level = ci.level,
    alternative = alternative,
    mu = mu,
    distribution = distribution,
    correct = correct,
    zero.method = zero.method,
    tol.root = tol.root
  )

  #-----------------------------------------------------------------------------
  # Prepare return
  #-----------------------------------------------------------------------------
  res[["method"]] <- paste0("Kornbrot's rank difference test using the ", res[["method"]])
  res[["alternative"]] <- gsub(
    pattern = "true location shift of differences",
    replacement = "True location shift of differences in ranks",
    x = res[["alternative"]],
    fixed = TRUE
  )
  res[["formula"]] <- formula
  res[["n"]][["original"]] <- data_prep[["n_original"]]
  res[["n"]][["nonmissing"]] <- n_nonmissing
  res[["n"]][["zero.adjusted"]] <- n_nonmissing - res[["n"]][["zeros"]]

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}
