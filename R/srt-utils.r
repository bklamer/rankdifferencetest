#-------------------------------------------------------------------------------
# Prepare data
#-------------------------------------------------------------------------------
srt_data_prep <- function(data, formula) {
  #-----------------------------------------------------------------------------
  # Parse formula
  #-----------------------------------------------------------------------------
  pf <- parse_formula(formula = formula, data = data)

  y_name <- pf[["y_name"]]
  x_name <- pf[["x_name"]]
  block_name <- pf[["block_name"]]

  # Check formula structure.
  # 1. Correct number of variables
  # 2. Functions that need eval
  # 3. No duplicated variables
  # 4. Variables must be in data frame
  if(length(y_name) > 1L) {
    if(y_name[1] %in% c("+", "-", "*", "^", "%in%", "/")) {
      stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
    } else {
      y_name <- deparse1(pf[["lhs"]][[2]])
      if(!y_name %in% names(data)) {
        data[[y_name]] <- eval(pf[["lhs"]][[2]], data)
      }
    }
  }

  if(length(x_name) > 1L) {
    if(x_name[1] %in% c("+", "-", "*", "^", "%in%", "/")) {
      stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
    } else {
      x_name <- deparse1(pf[["rhs"]][[2]])
      if(!x_name %in% names(data)) {
        data[[x_name]] <- eval(pf[["rhs"]][[2]], data)
      }
    }
  }

  if(length(block_name) > 1L) {
    if(x_name[1] %in% c("+", "-", "*", "^", "%in%", "/")) {
      stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
    } else {
      block_name <- deparse1(pf[["rhs_block"]][[2]])
      if(!block_name %in% names(data)) {
        data[[block_name]] <- eval(pf[["rhs_block"]][[2]], data)
      }
    }
  }

  if(!is.null(y_name)) {
    if(is.null(block_name)) {
      if(y_name == x_name) {
        stop(paste0("Check argument 'formula'. It contained a duplicated variable: ", deparse1(formula)))
      }
    } else {
      if(y_name == x_name || y_name == block_name || x_name == block_name) {
        stop(paste0("Check argument 'formula'. It contained a duplicated variable: ", deparse1(formula)))
      }
    }
  }

  if(!all(c(y_name, x_name, block_name) %in% names(data))) {
    missing_vars <- c(y_name, x_name, block_name)
    missing_vars <- missing_vars[!missing_vars %in% names(data)]
    stop(paste0(
      "Check argument 'formula'. It included variables not found in 'data': ",
      paste0(paste0("'", missing_vars, "'"),  collapse = ", ")
    ))
  }

  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  if(is.null(block_name)) {
    if(is.null(y_name)) {
      diffs <- data[[x_name]]
    } else {
      diffs <- data[[y_name]] - data[[x_name]]
    }
  } else {
    if(!is.factor(data[[x_name]])) {
      data[[x_name]] <- as.factor(data[[x_name]])
    }
    if(!is.factor(data[[block_name]])) {
      data[[block_name]] <- as.factor(data[[block_name]])
    }

    # Argument check: must be a 2-group comparison.
    if(length(levels(data[[x_name]])) != 2L) {
      stop(paste0(
        "Check argument 'data' and/or 'formula'. The variable '",
        x_name,
        "' must be a vector of class 'factor' with 2 levels."
      ))
    }

    spread <- reshape(
      data = data[c(y_name, x_name, block_name)],
      direction = "wide",
      idvar = block_name,
      timevar = x_name,
      v.names = y_name
    )

    names(spread) <- gsub(
      pattern = paste0(y_name, "."),
      replacement = "",
      x = names(spread),
      fixed = TRUE
    )

    y_name <- levels(data[[x_name]])[2L]
    x_name <- levels(data[[x_name]])[1L]

    diffs <- spread[[y_name]] - spread[[x_name]]
  }

  n_original <- length(diffs)
  diffs <- diffs[!is.na(diffs)]
  n_nonmissing <- length(diffs)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  list(
    diffs = diffs,
    y_name = y_name,
    x_name = x_name,
    n_original = n_original,
    n_nonmissing = n_nonmissing
  )
}

#-------------------------------------------------------------------------------
# Calculate signed rank test statistic
#-------------------------------------------------------------------------------
#' @importFrom stats setNames
srt_statistic <- function(
    diffs,
    mu,
    alternative,
    distribution,
    correct,
    zero.method,
    digits.rank
) {
  diffs <- diffs - mu
  if(is.finite(digits.rank)) {
    diffs <- signif(diffs, digits.rank)
  }

  are_zeros <- diffs == 0
  n_zeros <- sum(are_zeros)
  any_zeros <- n_zeros > 0L
  if(zero.method == "pratt") {
    n <- length(diffs)
  } else {
    n <- length(diffs) - n_zeros
  }

  if(all(are_zeros)) {
    stop("Data (or differences) are all zero. The Wilcoxon signed-rank test cannot be run.")
  }

  if(is.null(distribution)) {
    distribution <- if(n < 50) "exact" else "asymptotic"
  } else {
    if(n > 200 && distribution == "exact") {
      distribution <- "asymptotic"
      warning(
        paste0(
          "n > 200 Switching to ",
          dQuote("distribution = 'asymptotic'", q = FALSE),
          "."
        )
      )
    }
  }

  if(distribution == "exact") {
    if(any_zeros && zero.method == "wilcoxon") {
      diffs <- diffs[!are_zeros]
    }
    rank_abs_diffs <- rank(abs(diffs), na.last = "keep", ties.method = "average")
    if(any_zeros && zero.method == "pratt") {
      rank_abs_diffs <- rank_abs_diffs[!are_zeros]
      diffs <- diffs[!are_zeros]
    }
    any_ties <- length(rank_abs_diffs) != length(unique(rank_abs_diffs))
    if(any_ties) {
      ties <- ftabulate(rank_abs_diffs)
      n_ties <- sum(ties - 1L)
    } else {
      n_ties <- 0L
    }

    statistic <- setNames(sum(rank_abs_diffs[diffs > 0]), "W+")
  }

  if(distribution == "asymptotic") {
    if(any_zeros && zero.method == "wilcoxon") {
      diffs <- diffs[!are_zeros]
    }
    rank_abs_diffs <- rank(abs(diffs), na.last = "keep", ties.method = "average")
    w_plus <- sum(rank_abs_diffs[diffs > 0])
    e_w_plus <- n * (n + 1) / 4
    sd_w_plus <- n * (n + 1) * (2 * n + 1)
    if(any_zeros && zero.method == "pratt") {
      e_w_plus <- e_w_plus - (n_zeros * (n_zeros + 1) / 4)
      sd_w_plus <- sd_w_plus - (n_zeros * (n_zeros + 1) * (2 * n_zeros + 1))
      rank_abs_diffs <- rank_abs_diffs[!are_zeros]
    }
    any_ties <- length(rank_abs_diffs) != length(unique(rank_abs_diffs))
    if(any_ties) {
      ties <- ftabulate(rank_abs_diffs)
      n_ties <- sum(ties - 1L)
      ties <- sum(ties^3 - ties)
      sd_w_plus <- sd_w_plus - (ties / 2)
    } else {
      n_ties <- 0L
    }
    sd_w_plus <- sqrt(sd_w_plus / 24)

    statistic <- setNames((w_plus - e_w_plus) / sd_w_plus, "Z")

    # Continuity correction
    if(correct) {
      correction <- switch(
        alternative,
        two.sided = sign(w_plus) * 0.5,
        greater = 0.5,
        less = -0.5
      )
      statistic <- statistic - (correction / sd_w_plus)
    }

    statistic
  }

  # Return
  list(
    statistic = statistic,
    n = n,
    any_zeros = any_zeros,
    n_zeros = n_zeros,
    any_ties = any_ties,
    n_ties = n_ties,
    rank_abs_diffs = rank_abs_diffs,
    distribution = distribution,
    zero.method = zero.method,
    correct = correct
  )
}

#-------------------------------------------------------------------------------
# Calculate p-value
#-------------------------------------------------------------------------------
#' @importFrom exactRankTests pperm
#' @importFrom stats pnorm psignrank
srt_pval <- function(
    statistic,
    distribution,
    alternative,
    mu,
    n,
    any_zeros,
    n_zeros,
    any_ties,
    rank_abs_diffs,
    zero.method,
    correct,
    x_name,
    y_name
) {
  if(distribution == "exact") {
    method <- paste0(
      "exact Wilcoxon",
      if(zero.method == "pratt") "-Pratt" else NULL,
      " signed-rank test",
      if(any_zeros || any_ties) " using the Shift-algorithm" else NULL
    )

    if(any_ties || any_zeros) {
      if(zero.method == "pratt") {
        n <- n - n_zeros
      }
      # Shift-algorithm
      pval <- pperm(
        q = statistic,
        scores = rank_abs_diffs,
        m = n,
        alternative = alternative,
        pprob = FALSE
      )
    } else {
      # Standard Wilcoxon
      pval <- switch(
        alternative,
        two.sided = {
          p <- if(statistic > (n * (n + 1) / 4)) {
            psignrank(statistic - 1, n, lower.tail = FALSE)
          } else {
            psignrank(statistic, n)
          }
          min(2 * p, 1)
        },
        greater = psignrank(statistic - 1, n, lower.tail = FALSE),
        less = psignrank(statistic, n)
      )
    }
  }

  if(distribution == "asymptotic") {
    method <- paste0(
      "asymptotic Wilcoxon",
      if(zero.method == "pratt") "-Pratt" else NULL,
      " signed-rank test ",
      if(correct) "with" else "without",
      " continuity correction"
    )

    pval <- switch(
      alternative,
      two.sided = 2 * min(pnorm(statistic), pnorm(statistic, lower.tail = FALSE)),
      greater = pnorm(statistic, lower.tail = FALSE),
      less = pnorm(statistic)
    )
  }

  if(is.null(y_name)) {
    alternative <- paste0(
      "true center of '",
      x_name,
      "' is ",
      if(alternative == "two.sided") "not equal to " else paste0(alternative, " than "),
      mu
    )
  } else {
    alternative <- paste0(
      "true location shift of differences '",
      paste0(y_name, " - ", x_name),
      "' is ",
      if(alternative == "two.sided") "not equal to " else paste0(alternative, " than "),
      mu
    )
  }

  # Return
  list(
    pval = as.numeric(pval),
    method = method,
    alternative = alternative
  )
}

#-------------------------------------------------------------------------------
# Hodges-Lehmann estimator for one sample
#-------------------------------------------------------------------------------
hl <- function(diffs, return_sorted_walsh = FALSE) {
  walsh <- fouter_add(diffs, diffs)
  walsh <- walsh[lower.tri(walsh, diag = TRUE)] / 2

  if(return_sorted_walsh) {
    walsh <- sort(walsh)
    estimate <- list(
      estimate = fmedian(walsh, is.sorted = return_sorted_walsh),
      sorted_walsh = walsh
    )
  } else {
    estimate <- fmedian(walsh)
  }

  # Return
  estimate
}

#-------------------------------------------------------------------------------
# pseudomedian statistics for each method
#-------------------------------------------------------------------------------
srt_pseudomedian <- function(
    diffs,
    mu,
    n,
    alternative,
    distribution,
    ci.level,
    zero.method,
    rank_abs_diffs,
    any_zeros,
    n_zeros,
    any_ties,
    correct,
    digits.rank,
    tol.root
) {
  if(distribution == "exact") {
    if(any_zeros || any_ties) {
      if(zero.method == "pratt") {
        n <- n - n_zeros
      }
      pseudomedian <- srt_pseudomedian_shift(
        diffs = diffs,
        mu = mu,
        n = n,
        alternative = alternative,
        ci.level = ci.level,
        rank_abs_diffs = rank_abs_diffs,
        any_ties = any_ties
      )
    } else {
      pseudomedian <- srt_pseudomedian_wilcoxon(
        diffs = diffs,
        mu = mu,
        n = n,
        alternative = alternative,
        ci.level = ci.level
      )
    }
  }
  if(distribution == "asymptotic") {
    pseudomedian <- srt_pseudomedian_asymptotic(
      diffs = diffs,
      mu = mu,
      n = n,
      alternative = alternative,
      ci.level = ci.level,
      correct = correct,
      digits.rank = digits.rank,
      tol.root = tol.root
    )
  }

  pseudomedian
}

#-------------------------------------------------------------------------------
# pseudomedian statistics for each method
#-------------------------------------------------------------------------------
#' @importFrom stats psignrank qsignrank
srt_pseudomedian_wilcoxon <- function(diffs, mu, n, alternative, ci.level) {
  diffs <- diffs + mu
  alpha <- 1 - ci.level

  hl <- hl(diffs = diffs, return_sorted_walsh = TRUE)
  walsh <- hl[["sorted_walsh"]]

  cint <- switch(
    alternative,
    two.sided = {
      qu <- qsignrank(alpha / 2, n)
      if (qu == 0) qu <- 1
      ql <- n * (n + 1) / 2 - qu
      alpha.achieved <- 2 * psignrank(trunc(qu) - 1, n)
      c(walsh[qu], walsh[ql + 1])
    },
    greater = {
      qu <- qsignrank(alpha, n)
      if (qu == 0) qu <- 1
      alpha.achieved <- psignrank(trunc(qu) - 1, n)
      c(walsh[qu], +Inf)
    },
    less = {
      qu <- qsignrank(alpha, n)
      if (qu == 0) qu <- 1
      ql <- n * (n + 1) / 2 - qu
      alpha.achieved <- psignrank(trunc(qu) - 1, n)
      c(-Inf, walsh[ql + 1])
    }
  )

  if(alpha.achieved - alpha > alpha / 2) {
    warning("Requested 'ci.level' not achievable.")
  }

  ci.level.achieved <- 1 - signif(alpha.achieved, 2)
  estimate.method <- "Hodges-Lehmann estimator"
  ci.method <- "Quantiles of Walsh averages based on exact Wilcoxon signed-rank distribution"

  list(
    pseudomedian = hl[["estimate"]],
    lower = cint[1],
    upper = cint[2],
    ci.level.requested = ci.level,
    ci.level.achieved = ci.level.achieved,
    estimate.method = estimate.method,
    ci.method = ci.method,
    n = n,
    alternative = alternative
  )
}

#' @importFrom stats psignrank
#' @importFrom exactRankTests qperm
srt_pseudomedian_shift <- function(
    diffs,
    mu,
    n,
    rank_abs_diffs,
    any_ties,
    alternative,
    ci.level
) {
  diffs <- diffs + mu
  alpha <- 1 - ci.level

  hl <- hl(diffs = diffs, return_sorted_walsh = TRUE)
  walsh <- hl[["sorted_walsh"]]

  if(any_ties) {
    fs <- function(walsh, diffs) {
      xx <- diffs - walsh
      sum(rank(abs(xx))[xx > 0])
    }
    w <- sapply(walsh, fs, diffs = diffs)
  } else {
    w <- sum(rank(abs(diffs))):1
  }

  cint <- switch(
    alternative,
    two.sided = {
      qu <- qperm(alpha / 2, rank_abs_diffs, n)
      ql <- qperm(1 - alpha / 2, rank_abs_diffs, n)
      if(qu <= min(w)) {
        lci <- max(walsh)
      } else {
        lci <- min(walsh[w <= qu])
      }
      if(ql >= max(w)) {
        uci <- min(walsh)
      } else {
        uci <- max(walsh[w > ql])
      }
      c(uci, lci)
    },
    greater= {
      ql <- qperm(1-alpha, rank_abs_diffs, n)
      if(ql >= max(w)) {
        uci <- min(walsh)
      } else {
        uci <- max(walsh[w > ql])
      }
      c(uci, Inf)
    },
    less= {
      qu <- qperm(alpha, rank_abs_diffs, n)
      if(qu <= min(w)) {
        lci <- max(walsh)
      } else {
        lci <- min(walsh[w <= qu])
      }
      c(-Inf, lci)
    }
  )

  wmean <- sum(rank_abs_diffs) / 2
  estimate <- fmean(c(min(walsh[w <= ceiling(wmean)]), max(walsh[w > wmean])))
  estimate.method <- "Hodges-Lehmann estimator"
  ci.method <- "Quantiles of Walsh averages based on exact Wilcoxon signed-rank distribution using the Shift-algorithm"

  list(
    pseudomedian = estimate,
    lower = cint[1],
    upper = cint[2],
    ci.level.requested = ci.level,
    ci.level.achieved = NA_real_,
    estimate.method = estimate.method,
    ci.method = ci.method,
    n = n,
    alternative = alternative
  )
}

#' @importFrom stats uniroot qnorm
srt_pseudomedian_asymptotic <- function(
    diffs,
    mu,
    n,
    alternative,
    ci.level,
    correct,
    digits.rank,
    tol.root
) {
  diffs <- diffs + mu
  alpha <- 1 - ci.level
  ci.level.achieved <- NA_real_

  mumin <- min(diffs)
  mumax <- max(diffs)

  W <- function(d, diffs, correct, alternative, digits.rank) {
    xd <- diffs - d
    xd <- xd[xd != 0]
    nx <- length(xd)
    dr <- rank(abs(if(is.finite(digits.rank)) signif(xd, digits.rank) else xd))
    zd <- sum(dr[xd > 0]) - nx * (nx + 1) / 4
    n_ties.ci <- ftabulate(dr)
    sigma.ci <- sqrt(nx * (nx + 1) * (2 * nx + 1) / 24 - sum(n_ties.ci^3 - n_ties.ci) / 48)
    if(sigma.ci == 0) {
      warning("Cannot compute confidence interval when all observations are zero or tied.")
    }
    correction.ci <- if(correct) {
      switch(
        alternative,
        two.sided = sign(zd) * 0.5,
        greater = 0.5,
        less = -0.5
      )
    } else {
      0
    }
    (zd - correction.ci) / sigma.ci
  }

  estimate <- if(mumin == mumax) {
    mumin
  } else {
    uniroot(
      f = W, lower = mumin, upper = mumax, tol = tol.root,
      diffs = diffs, correct = correct, alternative = alternative,
      digits.rank = digits.rank
    )$root
  }

  wdiff <- function(d, zq, diffs, correct, alternative, digits.rank) {
    W(d = d,
      diffs = diffs,
      correct = correct,
      alternative = alternative,
      digits.rank = digits.rank) - zq
  }

  root <- function(
    zq,
    mumin,
    mumax,
    Wmumin,
    Wmumax,
    diffs,
    correct,
    alternative,
    digits.rank,
    tol.root
  ) {
    uniroot(
      f = wdiff, lower = mumin, upper = mumax,
      f.lower = Wmumin - zq, f.upper = Wmumax - zq, tol = tol.root,
      zq = zq, diffs = diffs, correct = correct, alternative = alternative,
      digits.rank = digits.rank
    )$root
  }

  Wmumin <- W(
    d = mumin,
    diffs = diffs,
    correct = correct,
    alternative = alternative,
    digits.rank = digits.rank
  )
  Wmumax <- if(!is.finite(Wmumin)) {
    NA
  } else {
    W(
      d = mumax,
      diffs = diffs,
      correct = correct,
      alternative = alternative,
      digits.rank = digits.rank
    )
  }

  if(!is.finite(Wmumax)) {
    cint <- c(
      if(alternative == "less") -Inf else NaN,
      if(alternative == "greater") +Inf else NaN
    )
    ci.level.achieved = 0
    estimate <- (mumin + mumax) / 2
    warning("Requested 'ci.level' not achievable.")
  } else {
    cint <- switch(
      alternative,
      two.sided = {
        repeat {
          mindiff <- Wmumin - qnorm(alpha / 2, lower.tail = FALSE)
          maxdiff <- Wmumax - qnorm(alpha / 2)
          if(mindiff < 0 || maxdiff > 0) {
            alpha <- alpha * 2
          } else {
            break
          }
        }
        if(alpha >= 1 || 1 - ci.level < alpha * 0.75) {
          ci.level.achieved <- 1 - pmin(1, alpha)
          warning("Requested 'ci.level' not achievable.")
        }
        if(alpha < 1) {
          l <- root(
            zq = qnorm(alpha / 2, lower.tail = FALSE),
            diffs = diffs,
            mumin = mumin,
            mumax = mumax,
            Wmumin = Wmumin,
            Wmumax = Wmumax,
            correct = correct,
            alternative = alternative,
            digits.rank = digits.rank,
            tol.root = tol.root
          )
          u <- root(
            zq = qnorm(alpha / 2),
            diffs = diffs,
            mumin = mumin,
            mumax = mumax,
            Wmumin = Wmumin,
            Wmumax = Wmumax,
            correct = correct,
            alternative = alternative,
            digits.rank = digits.rank,
            tol.root = tol.root
          )
          c(l, u)
        } else {
          rep(fmedian(diffs), 2)
        }
      },
      greater = {
        repeat {
          mindiff <- Wmumin - qnorm(alpha, lower.tail = FALSE)
          if(mindiff < 0) {
            alpha <- alpha * 2
          } else {
            break
          }
        }
        if(alpha >= 1 || 1 - ci.level < alpha * 0.75) {
          ci.level.achieved <- 1 - pmin(1, alpha)
          warning("Requested 'ci.level' not achievable.")
        }
        l <- if(alpha < 1) {
          root(
            zq = qnorm(alpha, lower.tail = FALSE),
            diffs = diffs,
            mumin = mumin,
            mumax = mumax,
            Wmumin = Wmumin,
            Wmumax = Wmumax,
            correct = correct,
            alternative = alternative,
            digits.rank = digits.rank,
            tol.root = tol.root
          )
        } else {
          fmedian(diffs)
        }
        c(l, +Inf)
      },
      less = {
        repeat {
          maxdiff <- Wmumax - qnorm(alpha/2)
          if(maxdiff > 0) {
            alpha <- alpha * 2
          } else {
            break
          }
        }
        if(alpha >= 1 || 1 - ci.level < alpha * 0.75) {
          ci.level.achieved <- 1 - pmin(1, alpha)
          warning("Requested 'ci.level' not achievable.")
        }
        u <- if(alpha < 1) {
          root(
            zq = qnorm(alpha),
            diffs = diffs,
            mumin = mumin,
            mumax = mumax,
            Wmumin = Wmumin,
            Wmumax = Wmumax,
            correct = correct,
            alternative = alternative,
            digits.rank = digits.rank,
            tol.root = tol.root
          )
        } else {
          fmedian(diffs)
        }
        c(-Inf, u)
      }
    )
  }

  estimate.method <- "pseudomedian based on root-finding algorithm for the asymptotic Wilcoxon signed-rank distribution"
  ci.method <- "quantiles based on root-finding algorithm for the asymptotic Wilcoxon signed-rank distribution"

  list(
    pseudomedian = estimate,
    lower = cint[1],
    upper = cint[2],
    ci.level.requested = ci.level,
    ci.level.achieved = ci.level.achieved,
    estimate.method = estimate.method,
    ci.method = ci.method,
    n = n,
    alternative = alternative
  )
}
