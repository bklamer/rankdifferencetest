#-------------------------------------------------------------------------------
# Prepare data
#-------------------------------------------------------------------------------
#' @importFrom stats complete.cases reshape
rdt_data_prep <- function(data, formula) {
  #-----------------------------------------------------------------------------
  # Parse formula
  #-----------------------------------------------------------------------------
  pf <- parse_formula(formula = formula, data = data)

  y_name <- pf[["y_name"]]
  x_name <- pf[["x_name"]]
  block_name <- pf[["block_name"]]

  # Check formula structure.
  # 1. Correct number of variables
  # 2. Functions that need eval
  # 3. No duplicated variables
  # 4. Variables must be in data frame
  if(length(y_name) == 0L) {
    stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
  }

  if(length(y_name) > 1L) {
    if(y_name[1] %in% c("+", "-", "*", "^", "%in%", "/")) {
      stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
    } else {
      y_name <- deparse1(pf[["lhs"]][[2]])
      data[[y_name]] <- eval(pf[["lhs"]][[2]], data)
    }
  }

  if(length(x_name) > 1L) {
    if(x_name[1] %in% c("+", "-", "*", "^", "%in%", "/")) {
      stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
    } else {
      x_name <- deparse1(pf[["rhs"]][[2]])
      data[[x_name]] <- eval(pf[["rhs"]][[2]], data)
    }
  }

  if(length(block_name) > 1L) {
    if(x_name[1] %in% c("+", "-", "*", "^", "%in%", "/")) {
      stop("Check argument 'formula'. It must strictly be of form 'y ~ x | block' or 'y ~ x'.")
    } else {
      block_name <- deparse1(pf[["rhs_block"]][[2]])
      data[[block_name]] <- eval(pf[["rhs_block"]][[2]], data)
    }
  }

  if(is.null(block_name)) {
    if(y_name == x_name) {
      stop(paste0("Check argument 'formula'. It contained a duplicated variable: ", deparse1(formula)))
    }
  } else {
    if(y_name == x_name || y_name == block_name || x_name == block_name) {
      stop(paste0("Check argument 'formula'. It contained a duplicated variable: ", deparse1(formula)))
    }
  }

  if(!all(c(y_name, x_name, block_name) %in% names(data))) {
    missing_vars <- c(y_name, x_name, block_name)
    missing_vars <- missing_vars[!missing_vars %in% names(data)]
    stop(paste0(
      "Check argument 'formula'. It included variables not found in 'data': ",
      paste0(paste0("'", missing_vars, "'"),  collapse = ", ")
    ))
  }

  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  if(is.null(block_name)) {
    y <- data[[y_name]]
    x <- data[[x_name]]

    # Argument check
    if(!(is.numeric(y) && is.numeric(x))) {
      x <- c(sQuote(y_name, FALSE), sQuote(x_name, FALSE))
      bad <- c(!is.numeric(y), !is.numeric(x))
      x <- paste(x[bad], collapse = " and ")
      stop(
        paste0(
          "In argument 'formula', variable ",
          x,
          " should have been numeric."
        )
      )
    }

    n_original <- length(y)

    ok <- complete.cases(x, y)
    y <- y[ok]
    x <- x[ok]
    n <- length(y)

    # Note: Kornbrot orders opposite direction: rank(-xtfrm(data)).
    ranks <- rank(c(y, x), na.last = "keep")

    y_ranks <- ranks[seq_len(n)]
    x_ranks <- ranks[seq_len(n) + n]
  } else {
    if(!is.factor(data[[x_name]])) {
      data[[x_name]] <- as.factor(data[[x_name]])
    }
    if(!is.factor(data[[block_name]])) {
      data[[block_name]] <- as.factor(data[[block_name]])
    }

    # Argument check
    if(!is.numeric(data[[y_name]])) {
      stop(
        paste0(
          "In argument 'formula', variable ",
          sQuote(y_name, FALSE),
          " should have been numeric."
        )
      )
    }

    # Argument check: must be a 2-group comparison.
    if(length(levels(data[[x_name]])) != 2L) {
      stop(paste0(
        "Check argument 'data' and/or 'formula'. The variable '",
        x_name,
        "' must be a vector of class 'factor' with 2 levels."
      ))
    }

    spread <- reshape(
      data = data[c(y_name, x_name, block_name)],
      direction = "wide",
      idvar = block_name,
      timevar = x_name,
      v.names = y_name
    )

    names(spread) <- gsub(
      pattern = paste0(y_name, "."),
      replacement = "",
      x = names(spread),
      fixed = TRUE
    )

    y_name <- levels(data[[x_name]])[2L]
    x_name <- levels(data[[x_name]])[1L]

    y <- spread[[y_name]]
    x <- spread[[x_name]]

    n_original <- length(y)

    ok <- complete.cases(x, y)
    y <- y[ok]
    x <- x[ok]
    n <- length(y)

    # Note: Kornbrot orders opposite direction: rank(-xtfrm(data)).
    ranks <- rank(c(y, x), na.last = "keep")

    y_ranks <- ranks[seq_len(n)]
    x_ranks <- ranks[seq_len(n) + n]
  }

  #-----------------------------------------------------------------------------
  # Prepare return
  #-----------------------------------------------------------------------------
  res <- list(y_ranks, x_ranks)

  attr(res, "names") <- c(y_name, x_name)
  attr(res, "row.names") <- .set_row_names(n)
  attr(res, "class") <- "data.frame"

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  list(
    data = res,
    n_original = n_original,
    n_nonmissing = n
  )
}
