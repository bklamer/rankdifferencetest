#' Signed-rank test
#'
#' Performs Wilcoxon's signed-rank test.
#'
#' The procedure for Wilcoxon's signed-rank test is as follows:
#'
#' 1. For one-sample data `x` or paired samples `x` and `y`, where `mu` is the
#'    measure of center under the null hypothesis, define the 'values' used for
#'    analysis as `(x - mu)` or `(x - y - mu)`.
#' 2. Define 'zero' values as `(x - mu == 0)` or `(x - y - mu == 0)`.
#'    - `zero.method = "wilcoxon"`: Remove values equal to zero.
#'    - `zero.method = "pratt"`: Keep values equal to zero.
#' 3. Order the absolute values from smallest to largest.
#' 4. Assign ranks \eqn{1, \dots, n} to the ordered absolute values, using
#'    mean rank for ties.
#' 5. `zero.method = "pratt"`: remove values equal to zero and their corresponding
#'    ranks.
#' 6. Calculate \eqn{W^+} as the sum of the ranks for positive values. The sum of
#'    \eqn{W^+} and \eqn{W^-} is \eqn{n(n+1)/2}, so either can be calculated from
#'    the other. If the null hypothesis is true, \eqn{W^+} and \eqn{W^-} are
#'    expected to be similar in value.
#' 7. Calculate the test statistic
#'    - `distribution = "exact"`: Use \eqn{W^+} as the test statistic. \eqn{W^+} takes
#'    values between 0 and \eqn{n(n+1)/2}. Under the null hypothesis, its
#'    expected mean and variance are \eqn{E_0(W^+) = n(n+1)/4} and
#'    \eqn{Var_0(W^+) = (n(n+1)(2n+1))/24}.
#'    - `distribution = "asymptotic"`: Let
#'    \eqn{Z=\frac{W^+ - E_0(W^+)}{Var_0(W^+)^{1/2}}}
#'    be the standardized version of \eqn{W^+}, then \eqn{Z \sim N(0, 1)}
#'    asymptotically. If there are ties, use the adjusted variance
#'    \eqn{Var_0(W^+) = \frac{n(n+1)(2n+1)}{24} - \frac{\sum(t^3 - t)}{48}},
#'    where \eqn{t} is the number of ties for each unique ranked absolute 'value'.
#'      - `zero.method = "pratt"`: The expected mean and variance are modified
#'      to be \eqn{E_0(W^+) = \frac{n(n+1)}{4} - \frac{(n_{zeros}(n_{zeros}+1)}{4}}
#'      and \eqn{Var_0(W^+) = \frac{n(n+1)(2n+1) - n_{zeros}(n_{zeros}+1)(2n_{zeros}+1)}{24}}.
#'      - `correct = TRUE`: For two-sided, greater than, and less than
#'      alternatives, define the continuity correction as \eqn{\text{sign}(W^+)0.5},
#'      \eqn{0.5}, and \eqn{-0.5}, respectively. \eqn{Z} is redefined as
#'      \eqn{Z = \frac{W^+ - E_0(W^+) - \text{correction}}{Var_0(W^+)^{1/2}}}.
#' 8. Calculate the p-value
#'    - `distribution = "exact"`: Use the Wilcoxon signed-rank distribution to calculate
#'    the probability of being as or more extreme than \eqn{W^+}.
#'      - When zeros or ties are present, use the Shift-Algorithm from
#'       \insertCite{streitberg1984;textual}{rankdifferencetest} as implemented
#'       in [exactRankTests::pperm()].
#'      - When zeros and ties are absent, use [stats::psignrank()].
#'    - `distribution = "asymptotic"`: Use the standard normal distribution to calculate the
#'    probability of being as or more extreme than \eqn{Z}. See [stats::pnorm()].
#'
#' `zero.method = "pratt"` uses the method by
#' \insertCite{pratt1959;textual}{rankdifferencetest}, which first
#' rank-transforms the absolute values, including zeros, and then removes the
#' ranks corresponding to the zeros. `zero.method = "wilcoxon"` uses the method
#' by \insertCite{wilcoxon1950;textual}{rankdifferencetest}, which first removes
#' the zeros and then rank-transforms the remaining absolute values.
#' \insertCite{conover1973;textual}{rankdifferencetest}
#' found that when comparing a discrete uniform distribution to a distribution
#' where probabilities linearly increase from left to right, Pratt's method
#' outperforms Wilcoxon's. When testing a binomial distribution centered at zero
#' to see whether the parameter of each Bernoulli trial is \eqn{\frac{1}{2}},
#' Wilcoxon's method outperforms Pratt's.
#'
#' When `ci = TRUE`, a pseudomedian and it's confidence interval are returned.
#' For exact tests, The Hodges-Lehman estimate is used and the confidence bounds
#' are estimated by calculating an appropriate quantile of the pairwise averages
#' (Walsh averages). For asymptotic tests, the pseudomedian is estimated using
#' [stats::uniroot()] to search for a root of the asymptotic normal approximation
#' of the Wilcoxon signed-rank distribution, with similar strategy for the
#' confidence bounds.
#'
#' The signed rank test traditionally assumes the values are independent and
#' identically distributed, with a continuous and symmetric distribution. The
#' hypotheses are stated as:
#' - Null: `(x)` or `(x - y)` is centered at `mu`.
#' - Two-sided alternative: `(x)` or `(x - y)` is not centered at `mu`.
#' - Greater than alternative: `(x)` or `(x - y)` is centered at a value greater
#'   than `mu`.
#' - Less than alternative: `(x)` or `(x - y)` is centered at a value less than
#'   `mu`.
#'
#' However, not all of these assumptions are required
#' \insertCite{pratt1981}{rankdifferencetest}. The 'identically distributed'
#' assumption is not required, keeping the level of test as expected for the
#' hypotheses as stated above. The symmetry assumption is not required when
#' using one-sided alternative hypotheses as:
#' - Null: `(x)` or `(x - y)` is symmetric and centered at `mu`.
#' - Greater than alternative: `(x)` or `(x - y)` is stochastically larger than
#'   `mu`.
#' - Less than alternative: `(x)` or `(x - y)` is stochastically smaller than
#'   `mu`.
#'
#' [stats::wilcox.test()] is the canonical function for the Wilcoxon signed-rank
#' test. Improvements and updated methods were introduced in
#' [exactRankTests::wilcox.exact()] and later [coin::wilcoxsign_test()].
#' [rankdifferencetest::srt()] attempts to refactor these functions so the best
#' features of each is available in a fast and easy to use format.
#'
#' @param data A data frame.
#' @param formula A formula of form:
#'        \describe{
#'          \item{y ~ x | block}{
#'            Use when `data` is in tall format. `y` is the numeric outcome,
#'            `x` is the binary explanatory variable, and `block` is the
#'            subject/item-level variable. If `x` is a factor, the first level
#'            will be the reference value. e.g.,
#'            `levels(data$x) <- c("pre", "post")` will result in the difference
#'            `post - pre`.
#'          }
#'          \item{y ~ x}{
#'            Use when `data` is in wide format. Differences are calculated
#'            as `data$y - data$x`.
#'          }
#'          \item{ ~ x}{
#'            Use when `data$x` represents pre-calculated differences or for the
#'            one sample case.
#'          }
#'        }
#' @param ci A scalar logical. Whether or not to calculate the pseudomedian and
#'        its confidence interval.
#' @param ci.level A scalar numeric from (0, 1). The confidence level.
#' @param alternative A string for the alternative hypothesis: `"two.sided"`
#'        (default), `"greater"`, or `"less"`.
#' @param mu A scalar numeric from (-Inf, Inf). Under the null hypothesis, `x`
#'        or `x - y` is assumed to be symmetric around `mu`.
#' @param distribution A string for the method used to calculate the p-value.
#'        If `"exact"`, the exact Wilcoxon signed-rank distribution is used.
#'        If `"asymptotic"`, the asymptotic normal approximation is used. The
#'        default (`NULL`) will automatically choose an appropriate method
#'        (`distribution = "exact"` when n < 50 or `distribution = "asymptotic"`
#'        otherwise).
#' @param correct A scalar logical. Whether or not to apply a continuity
#'        correction for the normal approximation of the p-value.
#' @param zero.method A string for the method used to handle values equal to
#'        zero: `"wilcoxon"` (default) or `"pratt"`.
#' @param tol.root A numeric scalar from (0, Inf). For
#' [stats::uniroot]`(*, tol=tol.root)` calls when `ci = TRUE` and
#'        `distribution = "asymptotic"`.
#' @param digits.rank A numeric scalar from (0, Inf]. If finite,
#'        [base::rank]`(`[base::signif]`(abs(diffs), digits.rank))` will be used to
#'        compute ranks for the test statistic instead of (the default)
#'        `rank(abs(diffs))`. e.g. `digits.rank = 7` can improve stability in
#'        determination of ties because they no longer depend on extremely small
#'        numeric differences.
#'
#' @seealso
#' [stats::wilcox.test()],
#' [coin::wilcoxsign_test()],
#' [rankdifferencetest::rdt()]
#'
#' @references
#' \insertRef{wilcoxon1950}{rankdifferencetest}
#'
#' \insertRef{pratt1981}{rankdifferencetest}
#'
#' \insertRef{pratt1959}{rankdifferencetest}
#'
#' \insertRef{cureton1967}{rankdifferencetest}
#'
#' \insertRef{conover1973}{rankdifferencetest}
#'
#' \insertRef{hollander2014}{rankdifferencetest}
#'
#' \insertRef{bauer1972}{rankdifferencetest}
#'
#' \insertRef{streitberg1984}{rankdifferencetest}
#'
#' \insertRef{hothorn2001}{rankdifferencetest}
#'
#' \insertRef{hothorn2002}{rankdifferencetest}
#'
#' \insertRef{hothorn2003}{rankdifferencetest}
#'
#' \insertRef{hothorn2008}{rankdifferencetest}
#'
#' @return
#' A list with the following elements:
#' \tabular{llll}{
#'   Slot \tab Subslot \tab Name \tab Description \cr
#'   1 \tab \tab `statistic` \tab Test statistic. \eqn{W^+} for the exact Wilcoxon signed-rank distribution or \eqn{Z} for the asymptotic normal approximation. \cr
#'   2 \tab \tab `p` \tab p-value. \cr
#'   3 \tab \tab `alternative` \tab The alternative hypothesis. \cr
#'   4 \tab \tab `method` \tab Method used for test results. \cr
#'   5 \tab \tab `formula` \tab Model formula. \cr
#'
#'   6 \tab   \tab `pseudomedian` \tab Measure of centrality for `x` or `y - x`. Not calculated when argument `ci = FALSE`. \cr
#'   6 \tab 1 \tab `estimate` \tab Estimated pseudomedian. \cr
#'   6 \tab 2 \tab `lower` \tab Lower bound of confidence interval for the pseudomedian. \cr
#'   6 \tab 3 \tab `upper` \tab Upper bound of confidence interval for the pseudomedian. \cr
#'   6 \tab 4 \tab `ci.level.requested` \tab The chosen `ci.level`. \cr
#'   6 \tab 5 \tab `ci.level.achieved` \tab For pathological cases, the achievable confidence level. \cr
#'   6 \tab 6 \tab `estimate.method` \tab Method used for calculating the pseudomedian. \cr
#'   6 \tab 7 \tab `ci.method` \tab Method used for calculating the confidence interval. \cr
#'
#'   7 \tab   \tab `n` \tab Number of observations \cr
#'   7 \tab 1 \tab `original` \tab The number of observations contained in `data`. \cr
#'   7 \tab 2 \tab `nonmissing` \tab The number of non-missing observations available for analysis. \cr
#'   7 \tab 3 \tab `zero.adjusted` \tab The number of non-missing and non-zero values available for analysis. i.e. `n$nonmissing - n$zeros`. \cr
#'   7 \tab 4 \tab `zeros` \tab The number of values that were zero. \cr
#'   7 \tab 5 \tab `ties` \tab The number of values that were tied.
#' }
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # srt() example
#' #----------------------------------------------------------------------------
#' library(rankdifferencetest)
#'
#' # Use example data from Kornbrot (1990)
#' data <- kornbrot_table1
#'
#' # The rate transformation inverts the rank ordering.
#' data$placebo_rate <- 60 / data$placebo
#' data$drug_rate <- 60 / data$drug
#'
#' # In contrast to the rank difference test, the Wilcoxon signed-rank test
#' # produces differing results. See table 1 and table 2 in Kornbrot (1990).
#' srt(
#'   formula = placebo ~ drug,
#'   data = data,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   correct = FALSE,
#'   zero.method = "wilcoxon",
#'   ci = TRUE
#' )
#' srt(
#'   formula = placebo_rate ~ drug_rate,
#'   data = data,
#'   alternative = "two.sided",
#'   distribution = "asymptotic",
#'   correct = FALSE,
#'   zero.method = "wilcoxon",
#'   ci = TRUE
#' )
#'
#' @importFrom exactRankTests pperm qperm
#'
#' @export
# Modified from stats::wilcox.test() v4.4.1 by R Core Team, GPL-2
srt <- function(
    data,
    formula,
    ci = FALSE,
    ci.level = 0.95,
    alternative = "two.sided",
    mu = 0,
    distribution = NULL,
    correct = TRUE,
    zero.method = "wilcoxon",
    tol.root = 1e-04,
    digits.rank = Inf
){
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  if(!is.data.frame(data)) {
    stop("Argument 'data' must be an object of class 'data.frame'.")
  }
  if(!inherits(formula, "formula")) {
    stop("Argument 'formula' must be an object of class 'formula'.")
  }
  if(!(is.logical(ci) && length(ci) == 1)) {
    stop("Argument 'ci' be a scalar vector of class 'logical'.")
  }
  if(ci) {
    if(
      !(is.numeric(ci.level) &&
        length(ci.level) == 1L &&
        ci.level > 0 &&
        ci.level < 1)
    ) {
      stop("Argument 'ci.level' must be a scalar vector of class 'numeric' between 0 and 1.")
    }
  }
  if(
    !(is.character(alternative) &&
      length(alternative) == 1L &&
      alternative %in% c("two.sided", "greater", "less"))
  ) {
    stop("Argument 'alternative' must be a scalar vector of class 'character'. Choose from 'two.sided', 'greater', or 'less'.")
  }
  if(!(is.numeric(mu) && length(mu) == 1L)) {
    stop("Argument 'mu' must be a scalar vector of class 'numeric'.")
  }
  if(!is.null(distribution)) {
    if(
      !(is.character(distribution) &&
        length(distribution) == 1L &&
        distribution %in% c("exact", "asymptotic"))
    ) {
      stop("Argument 'distribution' must be a scalar vector of class 'character'. Choose from 'exact' or 'asymptotic'.")
    }
  }
  if(!(is.logical(correct) && length(correct) == 1L)) {
    stop("Argument 'correct' must be a scalar vector of class 'logical'.")
  }
  if(
    !(is.character(zero.method) &&
      length(zero.method) == 1L &&
      zero.method %in% c("pratt", "wilcoxon"))
  ) {
    stop("Argument 'zero.method' must be a scalar vector of class 'character'. Choose from 'pratt' or 'wilcoxon'.")
  }
  if(!(is.numeric(tol.root) && length(tol.root) == 1L)) {
    stop("Argument 'tol.root' must be a scalar vector of class 'numeric'.")
  }

  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  data_prep <- srt_data_prep(data = data, formula = formula)

  diffs <- data_prep$diffs
  y_name <- data_prep$y_name
  x_name <- data_prep$x_name

  n_nonmissing <- data_prep[["n_nonmissing"]]
  if(n_nonmissing < 2L) {
    stop("The number of non-missing observations (sample size) was less than 2.")
  }

  #-----------------------------------------------------------------------------
  # Calculate test statistic
  #-----------------------------------------------------------------------------
  test_stat <- srt_statistic(
    diffs = diffs,
    mu = mu,
    alternative = alternative,
    distribution = distribution,
    correct = correct,
    zero.method = zero.method,
    digits.rank = digits.rank
  )

  statistic <- test_stat$statistic
  n <- test_stat$n
  any_zeros <- test_stat$any_zeros
  n_zeros <- test_stat$n_zeros
  any_ties <- test_stat$any_ties
  n_ties <- test_stat$n_ties
  rank_abs_diffs <- test_stat$rank_abs_diffs
  distribution <- test_stat$distribution

  #-----------------------------------------------------------------------------
  # Calculate p-value
  #-----------------------------------------------------------------------------
  pval <- srt_pval(
    statistic = statistic,
    distribution = distribution,
    alternative = alternative,
    mu = mu,
    n = n,
    any_zeros = any_zeros,
    n_zeros = n_zeros,
    any_ties = any_ties,
    rank_abs_diffs = rank_abs_diffs,
    zero.method = zero.method,
    correct = correct,
    x_name = x_name,
    y_name = y_name
  )

  p.value <- pval$pval
  method <- pval$method
  alternative_text <- pval$alternative

  #-----------------------------------------------------------------------------
  # Calculate confidence interval
  #-----------------------------------------------------------------------------
  if(ci) {
    ci <- srt_pseudomedian(
      diffs = diffs,
      mu = mu,
      n = n,
      alternative = alternative,
      distribution = distribution,
      ci.level = ci.level,
      zero.method = zero.method,
      rank_abs_diffs = rank_abs_diffs,
      any_zeros = any_zeros,
      n_zeros = n_zeros,
      any_ties = any_ties,
      correct = correct,
      digits.rank = digits.rank,
      tol.root = tol.root
    )

    pseudomedian <- ci$pseudomedian
    lower <- ci$lower
    upper <- ci$upper
    ci.level.requested <- ci$ci.level.requested
    ci.level.achieved <- ci$ci.level.achieved
    estimate.method <- ci$estimate.method
    ci.method <- ci$ci.method
  } else {
    pseudomedian <- NA_real_
    lower <- NA_real_
    upper <- NA_real_
    ci.level.requested <- NA_real_
    ci.level.achieved <- NA_real_
    estimate.method <- NA_character_
    ci.method <- NA_character_
  }

  #-----------------------------------------------------------------------------
  # Prepare return object
  #-----------------------------------------------------------------------------
  res <- list(
    statistic = statistic,
    p.value = p.value,
    alternative = alternative_text,
    method = method,
    formula = formula,
    pseudomedian = list(
      estimate = pseudomedian,
      lower = lower,
      upper = upper,
      ci.level.requested = ci.level.requested,
      ci.level.achieved = ci.level.achieved,
      estimate.method = estimate.method,
      ci.method = ci.method
    ),
    n = list(
      original = data_prep[["n_original"]],
      nonmissing = n_nonmissing,
      zero.adjusted = n_nonmissing - n_zeros,
      zeros = n_zeros,
      ties = n_ties
    )
  )

  class(res) <- c("rankdifferencetest", class(res))

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  res
}
